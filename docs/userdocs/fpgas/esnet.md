## ESnet SmartNIC Tools on Coder

The **ESnet FPGA SmartNIC template** enables the utilization of Xilinx Alveo FPGAs as SmartNICs, integrating seamlessly within the ESnet framework. This setup allows for high-performance networking tailored for scientific research.

### Overview of the ESnet SmartNIC Framework

The ESnet SmartNIC framework provides a complete workflow for programming AMD/Xilinx Alveo FPGA cards using **P4**. This open-source framework simplifies the integration of AMD/Xilinx tools and DPDK, facilitating easy programming of Alveo cards as SmartNICs.

### Workflows

1. **Development Workflow**: Compile your P4 programs into bitfiles using licensed Xilinx tools.
2. **Deployment Workflow**: Load the generated bitfiles onto Alveo cards for experimental use.

By following these workflows, you can effectively harness the capabilities of Alveo FPGAs within a high-performance networking environment. For more detailed guidance, refer to the full tutorial at [ESnet SmartNIC Tutorial](https://groundsada.github.io/esnet-smartnic-tutorial/).
