## Nautilus Glossary

This is an abridged list of terminology that Nautilus users may find useful. These glossary entries explain terminology used in the Nautilus User Documentation and Tutorials.

- <a id="artificialIntelligence"></a> **Artificial Intelligence** or **AI** refers to the development of computer systems that can perform tasks that typically require human intelligence. These tasks include learning from experience (machine learning), understanding natural language, recognizing patterns, solving problems, and making decisions. The goal of artificial intelligence is to create machines that can perform tasks intelligently, simulating human cognitive functions.

- <a id="command"></a> **Command** or **Unix command** is a text-based instruction or a program that is entered by a user in a Unix-like operating system's [command-line interface (CLI)](#commandLineInterface) to perform various tasks. Unix commands are used to interact with the operating system, manipulate files and directories, execute programs, manage processes, and perform system administration tasks. The command-line interface in Unix is often referred to as the shell.

- <a id="commandLineInterface"></a> **Command Line Interface** or **CLI** is a text-based user interface used to interact with a computer or software by entering commands into a terminal or console, often called a shell.

- **Container** See [Software Container](#softwareContainer)

- <a id="cuda"></a> **CUDA** CUDA stands for **C**ompute **U**nified **D**evice **Architecture. It is a parallel computing platform and programming model developed by NVIDIA for performing general-purpose computing on NVIDIA GPUs (Graphics Processing Units).

- <a id="deployment"></a> **Deployment** In Kubernetes, a Deployment is a resource object that manages the deployment and scaling of a set of replica [pods](#pods). Deployments provide a declarative way to define the desired state for an application and ensure that the current state matches the desired state. For example, if you wish to have three [pods](pods) running their services (i.e [software containers](#softwareContainer)) at all times, instead of launching three separate [jobs](jobs), you would launch a deployment with three replica pods.

- <a id="directory"></a> **Directory** In Unix-like operating systems, a directory is a file system container that holds files and other directories. It serves as a way to organize and structure the file system hierarchy. Directories are sometimes referred to as folders in other operating systems.

- <a id="forceDelete"></a> **Force Delete** In computer science, the term "force delete" typically refers to a method of deleting files or directories (folders) that cannot be removed through the usual or standard deletion process. It's important to exercise caution when using force delete commands, as they bypass certain safety checks and can result in the loss of important data. In [Kubernetes](#kubernetes), when you want to stop or delete a running pod (and its associated containers), you typically use the ```kubectl delete``` command.

- <a id="graphicalUserInterface"></a> **Graphical User Interface** or **GUI** is a type of user interface that allows users to interact with electronic devices or software through graphical elements such as icons, buttons, and windows. In a GUI, users can manipulate and control the system or application by using visual elements, typically with the help of a pointing device such as a mouse or a touchpad.

- <a id="GUI"></a> **GUI** See [Graphical User Interface](#graphicalUserInterface)

- <a id="initContainer"></a> **Init Container** In Kubernetes, an "**init container**" is a special type of container that runs before the main application containers in a pod. Init containers are used to perform setup or initialization tasks before the application containers start. They are designed to run to completion (i.e., run to success or failure) before the application containers begin their execution.

- <a id="job"></a> **Job** In [Kubernetes](#kubernetes), a job is a finite or batch task that runs to completion. Unlike other Kubernetes controllers like [Deployments](#deployment), which are designed to manage and maintain a desired number of replica pods continuously, a Job is intended for short-lived tasks that perform a specific job and then terminate.

- <a id="Kubernetes"></a>**Kubernetes** is an open-source container orchestration platform designed to automate the deployment, scaling, and management of containerized applications. Originally developed by Google and later donated to the Cloud Native Computing Foundation (CNCF), Kubernetes provides a robust and extensible platform for managing containerized workloads and services.

- <a id="machineLearning"></a> **Machine Learning** or **ML** is a subset of [artificial intelligence (AI)](#artificialIntelligence) that focuses on the development of algorithms and statistical models that enable computers to perform tasks without explicit programming. The primary goal of machine learning is to enable computers to learn from data and improve their performance over time without human intervention.

- <a id="namespace"></a> **Namespace** In [Kubernetes](#Kubernetes), a namespace is a virtual cluster within the physical cluster. It provides a way to partition and organize resources within a Kubernetes cluster, allowing multiple users, teams, or projects to share the same cluster without interfering with each other. Namespaces help in creating isolated environments and preventing naming conflicts between different resources.

- <a id="nautilus"></a> **Nautilus** is a distributed, hyper-converged GPU/CPU cluster made of heterogenous nodes that include a variety of GPU cards, CPUs, storage and network.

- <a id="orchestration"></a> **Orchestration** In Kubernetes, orchestration refers to the automated coordination and management of containerized applications, their deployment, scaling, and operation within a cluster, such as Nautilus.

- <a id="persistentVolumeClaim"></a> **Persistent Volume Claim** or **PVC** is a resource that represents a request for storage by a user or a pod. It is part of the storage abstraction in Kubernetes, providing a way for applications to request and use persistent storage resources without needing to know the details of the underlying storage infrastructure. PVCs can be requested by a pod, created by a user, mounted easily to a container and shared by multiple users or processes.

- <a id="pod"></a> **Pod** In Kubernetes, a pod is the smallest and simplest unit in the deployment model. A pod represents a single instance of a running process in a cluster, and it encapsulates one or more containerized applications. At a minimum, a pod must run some kind of operating system, such as Ubuntu Linux.

- <a id="service"></a> A **Service** is an abstraction that defines a logical set of pods and a policy by which to access them. Services enable a dynamic and scalable way for different parts of an application to communicate with each other within a Kubernetes cluster. For example, they may provide a stable endpoint (IP address and DNS name) that allows other services or external users to reach the pods associated with the service.

- <a id="shell"></a> **Shell** In Unix, a shell is a command-line interpreter or a command processor that provides a user interface for interacting with the Unix or Unix-like operating systems (e.g Mac OS X). The shell is a crucial component of the operating system, responsible for interpreting user commands, executing programs, and managing the interaction between the user and the operating system kernel.

- <a id="softwareContainer"></a> **Software Container** A software container is a lightweight, standalone, and executable package that includes everything needed to run a piece of software, including the code, runtime, system tools, libraries, and settings. Containers provide a consistent and reproducible environment, ensuring that the software runs consistently across different computing environments.

- **Stateless** In computer engineering and software development, the term *stateless* refers to a system or application that does not retain information about the state or status of a user or entity between sessions or interactions. In the case of Nautilus, [pods](#pod) are stateless - each request to instance a pod is treated as an independent and self-contained transaction, and the cluster does not store any information about the previous pod or its characteristics.

- <a id="unix"></a> **Unix** is a powerful and versatile multiuser, multitasking operating system originally developed in the 1960s and 1970s at Bell Labs (AT&T). It is known for its stability, security, and support for a wide range of applications and services. Unix has had a significant impact on the development of operating systems and has influenced many modern operating systems, including Linux and macOS.
